#include "modbus.h"

// Global Variables
uint8_t modbusState = MODBUS_IDLE;
uint8_t modbusAddress = MODBUS_DEFAULT_ADDRESS;
#if _EEPROMSIZE != 0 && !_PIC18
__eeprom uint8_t modbusSavedAddress = MODBUS_DEFAULT_ADDRESS;
#endif
#ifdef MODBUS_SUPPORT_READ_INPUT_REGISTERS
uint16_t modbusInputRegisters[MODBUS_INPUT_REGISTER_COUNT];
#endif
#ifdef MODBUS_SUPPORT_PRESET_MULTIPLE_REGISTERS || \
    MODBUS_SUPPORT_PRESET_SINGLE_REGISTER || \
    MODBUS_SUPPORT_READ_HOLDING_REGISTERS
uint16_t modbusHoldingRegisters[MODBUS_HOLDING_REGISTER_COUNT];
#endif

// Local Constants & Definitions
enum modbusRxState {
    ADDRESS, // Waiting for address
    NOT_ADDRESSED, // Address mismatch 
    FUNCTION,
    START_HI,
    START_LO,
    NUMBER_HI,
    NUMBER_LO,
    WRITE_SIZE,
    WRITE_BUFFERING,
    CRC_LO,
    CRC_HI
};

enum functionCode {
    READ_COIL_STATUS = 1,
    READ_INPUT_STATUS = 2,
    READ_HOLDING_REGISTERS = 3,
    READ_INPUT_REGISTERS = 4,
    FORCE_SINGLE_COIL = 5,
    PRESET_SINGLE_REGISTER = 6,
    FORCE_MULTIPLE_COILS = 15,
    PRESET_MULTIPLE_REGISTERS = 16
};

// Local Variables
uint8_t rxState = ADDRESS;
#ifdef MODBUS_SUPPORT_PRESET_MULTIPLE_REGISTERS
uint8_t modbusWriteBuffer[MODBUS_MAX_WRITE_SIZE]; // only payload
#endif
uint8_t modbusRespBuffer[MODBUS_MAX_RESP_SIZE + 5]; // the whole message
uint16_t rxCrcHolder = 0xffff;
uint16_t txCrcHolder;
uint8_t reqFunction;
uint16_t reqStartIndex, reqAccessCount, reqCrc;
uint8_t reqWriteSize, reqWriteIndex;
uint16_t crcFailCounter = 0;
uint8_t modbusTxByteCounter;
uint8_t *txBytePtr;

// Global Functions

void modbusInit(void) {
    #if _EEPROMSIZE != 0 && !_PIC18
    modbusAddress = modbusSavedAddress;
    #endif
}

void modbusTimeout(void) {
    MODBUS_TIMER = MODBUS_TIMER_PRELOAD;
    rxState = ADDRESS;
    rxCrcHolder = 0xffff;
}

void modbusProcessRxByte(uint8_t rxByte) {
    MODBUS_TIMER = MODBUS_TIMER_PRELOAD;
    switch (rxState) {
        case CRC_HI:
        case CRC_LO:
        case NOT_ADDRESSED:
            break;
        default:
            rxCrcHolder = crc16(rxCrcHolder, rxByte);
    }

    switch (rxState) {
        case ADDRESS:
            if (rxByte == modbusAddress)
                rxState = FUNCTION;
            else
                rxState = NOT_ADDRESSED;
            break;
        case FUNCTION:
            reqFunction = rxByte;
            rxState = START_HI;
            break;
        case START_HI:
            reqStartIndex = (uint16_t) rxByte << 8;
            rxState = START_LO;
            break;
        case START_LO:
            reqStartIndex += rxByte;
            rxState = NUMBER_HI;
            break;
        case NUMBER_HI:
            reqAccessCount = (uint16_t) rxByte << 8;
            rxState = NUMBER_LO;
            break;
        case NUMBER_LO:
            reqAccessCount += rxByte;
            if (reqFunction == 15 || reqFunction == 16)
                rxState = WRITE_SIZE;
            else
                rxState = CRC_LO;
            break;
        case WRITE_SIZE:
            reqWriteSize = rxByte;
            reqWriteIndex = 0;
            rxState = WRITE_BUFFERING;
            break;
        case WRITE_BUFFERING:
            #ifdef MODBUS_SUPPORT_PRESET_MULTIPLE_REGISTERS
            modbusWriteBuffer[reqWriteIndex++] = rxByte;
            #endif
            if (reqWriteIndex == reqWriteSize)
                rxState = CRC_LO;
            break;
        case CRC_LO:
            reqCrc = (uint16_t) rxByte;
            rxState = CRC_HI;
            break;
        case CRC_HI:
            reqCrc += (uint16_t) rxByte << 8;
            if (rxCrcHolder == reqCrc)
                modbusState = MODBUS_REQUEST_WAITING;
            else {
                modbusState = MODBUS_IDLE;
                ++crcFailCounter;
            }
            rxState = ADDRESS;
            rxCrcHolder = 0xffff;
            break;
    }
}

void modbusProcessRequest(void) {
    uint8_t i, bytesExcludingCrc;
    uint8_t *ptr;
    modbusRespBuffer[0] = modbusAddress;
    switch (reqFunction) {

        #ifdef MODBUS_SUPPORT_READ_INPUT_REGISTERS
        case READ_INPUT_REGISTERS:
            modbusRespBuffer[1] = reqFunction;
            modbusRespBuffer[2] = reqAccessCount * 2;
            ptr = (uint8_t*) (&modbusInputRegisters[reqStartIndex]);
            for (i = 0; i < reqAccessCount; ++i) {
                modbusRespBuffer[2 * i + 3] = *(ptr + 1);
                modbusRespBuffer[2 * i + 4] = *ptr;
                ptr += 2;
            }
            bytesExcludingCrc = (reqAccessCount * 2) + 3;
            break;
        #endif

        #ifdef MODBUS_SUPPORT_READ_HOLDING_REGISTERS
        case READ_HOLDING_REGISTERS:
            modbusRespBuffer[1] = reqFunction;
            modbusRespBuffer[2] = reqAccessCount * 2;
            ptr = (uint8_t*) (&modbusHoldingRegisters[reqStartIndex]);
            for (i = 0; i < reqAccessCount; ++i) {
                modbusRespBuffer[2 * i + 3] = *(ptr + 1);
                modbusRespBuffer[2 * i + 4] = *ptr;
                ptr += 2;
            }
            bytesExcludingCrc = (reqAccessCount * 2) + 3;
            break;
        #endif
            
        #ifdef MODBUS_SUPPORT_PRESET_SINGLE_REGISTER
        case PRESET_SINGLE_REGISTER:
            modbusHoldingRegisters[reqStartIndex] = reqAccessCount;
            modbusRespBuffer[1] = reqFunction;
            ptr = (uint8_t*) &reqStartIndex;
            modbusRespBuffer[2] = *(ptr + 1);
            modbusRespBuffer[3] = *ptr;
            ptr = (uint8_t*) &reqAccessCount;
            modbusRespBuffer[4] = *(ptr + 1);
            modbusRespBuffer[5] = *ptr;
            bytesExcludingCrc = 6;
            break;
        #endif
            
        #ifdef MODBUS_SUPPORT_PRESET_MULTIPLE_REGISTERS
        case PRESET_MULTIPLE_REGISTERS:
            ptr = (uint8_t*) (&modbusHoldingRegisters[reqStartIndex]);
            for (i = 0; i < reqAccessCount; ++i) {
                *(ptr + 1) = modbusWriteBuffer[i * 2];
                *ptr = modbusWriteBuffer[i * 2 + 1];
                ptr += 2;
            }
            modbusRespBuffer[1] = reqFunction;
            ptr = (uint8_t*) &reqStartIndex;
            modbusRespBuffer[2] = *(ptr + 1);
            modbusRespBuffer[3] = *ptr;
            ptr = (uint8_t*) &reqAccessCount;
            modbusRespBuffer[4] = *(ptr + 1);
            modbusRespBuffer[5] = *ptr;
            bytesExcludingCrc = 6;
            break;
        #endif
            
        default: // Unsupported message
            modbusRespBuffer[1] = reqFunction + 0x80;
            modbusRespBuffer[2] = 1; // Error: Illegal Function
            bytesExcludingCrc = 3;
            break;
    }

    // Calculating CRC
    txCrcHolder = 0xffff;
    for (i = 0; i < bytesExcludingCrc; ++i)
        txCrcHolder = crc16(txCrcHolder, modbusRespBuffer[i]);
    modbusRespBuffer[bytesExcludingCrc] = txCrcHolder;
    modbusRespBuffer[bytesExcludingCrc + 1] = txCrcHolder >> 8;

    // Enabling interrupt based TX
    modbusTxByteCounter = bytesExcludingCrc + 2;
    txBytePtr = modbusRespBuffer;
    TXIE = 1;

    // Request is served
    modbusState = MODBUS_IDLE;
}

void modbusTxInterrupt(void) {
    TXREG = *txBytePtr;
    ++txBytePtr;
    if (--modbusTxByteCounter == 0) TXIE = 0;
}

uint16_t crc16(uint16_t oldCrc, uint8_t newByte) {
    uint16_t crc = oldCrc ^ newByte;
    uint8_t i;
    for (i = 8; i != 0; --i) {
        if ((crc & 0x0001) != 0) {
            crc >>= 1;
            crc ^= 0xa001;
        } else
            crc >>= 1;
    }
    return crc;
}
