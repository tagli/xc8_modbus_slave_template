#include "config.h"
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "modbus.h"

// Definitions & Constants
#define _XTAL_FREQ 4000000ULL
#define DEBUG_LED PORTBbits.RB4

// Local Function Prototypes
void initialize(void);
void interrupt isr(void);

void interrupt isr(void) {
    if (TMR0IE && TMR0IF) {
        modbusTimeout();
        TMR0IF = 0;
    }
    if (RCIE && RCIF) {
        modbusProcessRxByte(RCREG);
    }
    if (TXIE && TXIF) {
        modbusTxInterrupt();
    }
}

void main(void) {

    initialize();
    
    while (1) {
        if (modbusState == MODBUS_REQUEST_WAITING) {
            modbusProcessRequest();

            // Address change?
            modbusAddress = modbusHoldingRegisters[0];
            if (modbusAddress != modbusSavedAddress) {
                modbusSavedAddress = modbusAddress;
                while(EECON1bits.WR);
            }
        }
    }
    
}

void initialize(void) {
    
    // Pin Settings
    PORTA = 0;
    PORTB = 0;
    TRISBbits.TRISB4 = 0; // for DEBUG_LED
    CMCONbits.CM = 0b111; // Comparators are disabled
    
    // Oscillator Settings
    OSCCONbits.IRCF = 0b110; // 4 MHz
    while (OSCCONbits.IOFS == 0);
    
    // USART Settings
    BRGH = 1;
    SPBRG = 25; // 9600 bps @ 4 MHz
    TX9D = 1; // used as 2nd stop bit
    TX9 = 1;
    TXEN = 1;
    RX9 = 1;
    CREN = 1;
    SPEN = 1;
    
    // Modbus Initialization
    modbusInit();
    modbusHoldingRegisters[0] = modbusAddress & 0x00ff;
    
    // TMR0 Settings
    OPTION_REGbits.PSA = 0;
    OPTION_REGbits.PS = 0b011; // TMR0 prescaler: 1/16
    
    // Interrupt Settings
    RCIE = 1;
    TMR0IE = 1;
    PEIE = 1;
    GIE = 1;
    
    // Initialization Done Signal
    DEBUG_LED = 1;
    __delay_ms(500);
    DEBUG_LED = 0;
}
