#ifndef MODBUS_H
#define	MODBUS_H

#include <xc.h>
#include <stdint.h>

// Global Constants & Settings
#define MODBUS_DEFAULT_ADDRESS 1
#define MODBUS_TIMER_PRELOAD 4 // for 3.5 byte-time timeout
#define MODBUS_TIMER TMR0
#define MODBUS_MAX_WRITE_SIZE 10 // for multi write operations, in bytes
#define MODBUS_MAX_RESP_SIZE 10 // for multi-byte responses, in bytes
#define MODBUS_INPUT_REGISTER_COUNT 2
#define MODBUS_HOLDING_REGISTER_COUNT 2

// Supported Message Types
#define MODBUS_SUPPORT_READ_HOLDING_REGISTERS
#define MODBUS_SUPPORT_READ_INPUT_REGISTERS
#define MODBUS_SUPPORT_PRESET_SINGLE_REGISTER
#define MODBUS_SUPPORT_PRESET_MULTIPLE_REGISTERS

enum modbusReqState {
    MODBUS_IDLE,
    MODBUS_REQUEST_WAITING
};

// Global Variables
extern uint8_t modbusState;
extern uint8_t modbusAddress;
#if _EEPROMSIZE != 0 && !_PIC18
extern __eeprom uint8_t modbusSavedAddress;
#endif
#ifdef MODBUS_SUPPORT_READ_INPUT_REGISTERS
extern uint16_t modbusInputRegisters[];
#endif
#ifdef MODBUS_SUPPORT_PRESET_MULTIPLE_REGISTERS || \
    MODBUS_SUPPORT_PRESET_SINGLE_REGISTER || \
    MODBUS_SUPPORT_READ_HOLDING_REGISTERS
extern uint16_t modbusHoldingRegisters[];
#endif

// Global Functions
void modbusInit(void);
void modbusTimeout(void);
void modbusTxInterrupt(void);
void modbusProcessRxByte(uint8_t rxByte);
void modbusProcessRequest(void);
uint16_t crc16(uint16_t oldCrc, uint8_t newByte);

#endif	// MODBUS_H

